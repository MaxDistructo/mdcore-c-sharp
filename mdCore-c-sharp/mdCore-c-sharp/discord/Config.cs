﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace mdCore_c_sharp.discord
{
    class Config
    {
        static String filePath = "config.json";
        public static String readPrefix() {
            return JSONUtils.readJSONFromFile(filePath).getString("prefix");
        }
        public static String readToken() {
            return JSONUtils.readJSONFromFile(filePath).getString("token");
        }
        public static String readOwnerId() {
            return JSONUtils.readJSONFromFile(filePath).getString("owner_id");
        }
        public static void SetFilePath(String filepath) {
            filePath = filepath;
        }
    }
}
