﻿using System;
using System.IO;

namespace mdCore_c_sharp
{
    class FileUtils
    {
        public static String[] readFileLines(String path) {
            return File.ReadAllLines(Constants.getCurrentDirectory() + path);
        }
        public static String readFile(String path) {
            Console.WriteLine("Reading File: " + Constants.getCurrentDirectory() + path);
            return File.ReadAllText(Constants.getCurrentDirectory() + path);
        }
        public static void writeFile(String path, String data) {
            File.WriteAllText(Constants.getCurrentDirectory() + path, data);
        }
        public static void appendFile(String path, String data) {
            using (StreamWriter file =
                new StreamWriter(Constants.getCurrentDirectory() + path, true))
            {
                file.WriteLine(data);
            }
        }

    }
}
