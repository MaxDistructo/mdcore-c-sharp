﻿using mdCore_c_sharp.json;
using System;
//using Newtonsoft.Json;

namespace mdCore_c_sharp
{
    class JSONUtils
    {
        public static JSONObject readJSONFromFile(String filename) {
            return new JSONObject(FileUtils.readFile(filename));
        }
        public static void writeJSONToFile(String filename, JSONObject json) {
            FileUtils.writeFile(filename, json.ToString());
        }
        public static void createNewJSONFile(String filename) {
            FileUtils.writeFile(filename, "{}");
        }
    }
}
