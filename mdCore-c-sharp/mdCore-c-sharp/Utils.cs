﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mdCore_c_sharp
{
    class Utils
    {
        public static String makeNewString(List<object> input, int startAt) {
            String ret = "";
            foreach (String str in input) {
                ret += str + " ";
            }
            return ret;
        }
        public static long convertToLong(object o) {
            return Convert.ToInt64(o.ToString());
        }
        public static int convertToInt(object o) {
            return Convert.ToInt32(o.ToString());
        }
        public static String makeNewLineString(List<String> input, int startAt) {
            String ret = "";
            foreach (String str in input) {
                ret += str + "\n";
            }
            return ret;
        }
        public static List<String> arrayToList(String[] arr) {
            return arr.ToList<String>();
        }
        public static String[] listToArray(List<String> list) {
            return list.ToArray();
        }
    }
}
