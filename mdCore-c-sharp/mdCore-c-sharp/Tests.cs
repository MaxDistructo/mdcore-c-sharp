﻿using mdCore_c_sharp.json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mdCore_c_sharp
{
    class Tests
    {
        public static void runTests() {
            Console.WriteLine("Testing JSON Implemenation");
            testJSONImplementation();
            Console.WriteLine("JSON Test Complete");
        }
        public static void testJSONImplementation() {
            //Quick Test JSON. Tests String, Int, Long, Array, and Boolean
            String test_string = "{\"string\":\"value\", \"solo_int\":1, \"long\":12345678901234567890123456, \"int_array\":[1,2,3,4,5,6],\"bool\":true}";
            JSONObject json = new JSONObject(test_string);
            Console.WriteLine(json.toString());
        }
    }
}
