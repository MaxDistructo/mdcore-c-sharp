﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace mdCore_c_sharp.json
{
    class JSONException : Exception
    {
        public JSONException() : base("Invalid JSON Get") { }

        public JSONException(string message) : base(message)
        {
        }

        public JSONException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected JSONException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
