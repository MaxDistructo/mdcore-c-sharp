﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mdCore_c_sharp.json
{
    class JSONEntry
    { 
        public Type data_type;
        public object data;
        public JSONEntry(object data){
            this.data_type = data.GetType();
            this.data = data;
        }
        public object get() {
            return data;
        }
        public Type getDataType() {
            return data_type;
        }
        public String toString() {
            return data_type + " : " + data;
        }
       
    }
}
