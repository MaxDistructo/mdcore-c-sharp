﻿using System;
using System.Collections.Generic;
using System.Linq;
using static mdCore_c_sharp.Utils;

namespace mdCore_c_sharp.json
{
    class JSONObject
    {
        Dictionary<String, JSONEntry> dict = new Dictionary<String, JSONEntry>();
        public JSONObject() {
            dict = new Dictionary<String, JSONEntry>();
        }

        public JSONObject(String json) {
            this.dict = this.desterilizeJSONObjectDict(json, 0).Key;
        }

        public JSONObject(Dictionary<String, JSONEntry> dict) {
            this.dict = dict;
        }

        public object get(String key) {
            JSONEntry entry;
            bool result = dict.TryGetValue(key, out entry);
            if (result) {
                return entry.data;
            }
            else {
                return null;
            }
        }

        public Int32 getInt32(String key) {
            JSONEntry entry;
            bool result = dict.TryGetValue(key, out entry);
            if (result)
            {
                return Convert.ToInt32(entry.data);
            }
            else
            {
                throw new JSONException(); //Throw JSON Exception
            }
        }

        public Int64 getInt64(String key)
        {
            JSONEntry entry;
            bool result = dict.TryGetValue(key, out entry);
            if (result)
            {
                return Convert.ToInt64(entry.data);
            }
            else
            {
                throw new JSONException(); //Throw JSON Exception
            }
        }

        public bool getBool(String key)
        {
            JSONEntry entry;
            bool result = dict.TryGetValue(key, out entry);
            if (result)
            {
                return Convert.ToBoolean(entry.data);
            }
            else
            {
                throw new JSONException(); //Throw JSON Exception
            }
        }

        public String getString(String key)
        {
            JSONEntry entry;
            bool result = dict.TryGetValue(key, out entry);
            if (result)
            {
                return entry.data.ToString();
            }
            else
            {
                throw new JSONException(); //Throw JSON Exception
            }
        }

        public void put(String key, object entry) {
            dict.Add(key, new JSONEntry(entry));
        }

        public void remove(String key) {
            dict.Remove(key);
        }

        KeyValuePair<Dictionary<String, JSONEntry>, int> desterilizeJSONObjectDict(String json, int startAt) {
            Dictionary<String, JSONEntry> dict = new Dictionary<String, JSONEntry>();
            bool skip_first = true;
            bool key = true;
            String key_str = "";
            JSONEntry value = new JSONEntry("");
            int i = startAt;
            char[] nums = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            Console.WriteLine("Desterilizing JSON: " + json);
            while (i < json.Length) {
                char character = json[i];
                //Console.WriteLine(key_str);
                //Console.WriteLine(value.toString());
                //Skip Char 1 as it is the start of this object
                if (skip_first)
                {
                    skip_first = false;
                }
                else {
                    Console.WriteLine("Current Char: " + character);
                    if (character == '"') {
                        if (key)
                        {
                            KeyValuePair<String, int> tmp = desterilizeString(json, i);
                            Console.WriteLine("KEY: "+ tmp.Key);
                            key_str = tmp.Key;
                            i = tmp.Value;
                        }
                        else {
                            KeyValuePair<String, int> tmp = desterilizeString(json, i);
                            Console.WriteLine("VALUE: " + tmp.Key);
                            value.data = tmp.Key;
                            value.data_type = value.data.GetType();
                            i = tmp.Value;
                        }
                    }
                    if (character == ':') {
                        Console.WriteLine("Setting Value Mode");
                        key = false;
                    }
                    if (character == '{') {
                        KeyValuePair<Dictionary<String, JSONEntry>, int> tmp = desterilizeJSONObjectDict(json, i);
                        value.data = tmp.Key;
                        value.data_type = value.data.GetType();
                        i = tmp.Value;
                    }
                    if (character == '}') {
                        dict.Add(key_str, value); //Add final entry before returning.
                        return new KeyValuePair<Dictionary<String, JSONEntry>, int>(dict, i); //Break Loop and Return this JSONObject
                    }
                    if (character == ',') {
                        key = true;
                        Console.WriteLine("Writing to dict");
                        try
                        {
                            dict.Add(key_str, value);
                            key_str = "";
                            value = new JSONEntry("");
                        }
                        catch (System.ArgumentException e) {
                            Console.WriteLine("Error adding Entry: Item with Same Key Already Exists.");
                            Console.WriteLine("Current Dict: " + dict.ToString());
                        }
                    }
                    if (character == 'f') {
                        value.data = false;
                        value.data_type = value.data.GetType();
                        i += 4; //Rather than fully interpret the true/false words, take first letter and jump forward
                    }
                    if (character == 't') {
                        value.data = true;
                        value.data_type = value.data.GetType();
                        i += 3;
                    }
                    if (nums.Contains(character)){
                        KeyValuePair<String, int> tmp = desterilizeNumber(json, i);
                        i = tmp.Value;
                        if (tmp.Key.Count() > 10 || tmp.Key.Contains('l') || tmp.Key.Contains('L')) {
                            value.data_type = 1L.GetType();
                            value.data = tmp.Key;

                        }
                        else {
                            value.data_type = 1.GetType();
                            value.data = tmp.Key;
                        }
                    }
                    if (character == '[') {
                        KeyValuePair<List<String>, int> tmp = desterilizeArray(json, i);
                        value.data = tmp.Key;
                        value.data_type = value.data.GetType();
                        i = tmp.Value;
                    }
                }
                i++;
            }
            return new KeyValuePair<Dictionary<String, JSONEntry>, int>(dict, i);
        }

        KeyValuePair<List<String>, int> desterilizeArray(String json, int startAt) {
            Console.WriteLine("Attempting Desterilization of Array starting from " + startAt);
            List<String> array = new List<String>();
            int i = startAt + 1;
            String ret = "";
            while (json[i] != ']') { //Loop until the end of the Array
                if (json[i] != ',') //Loop over each character till the array element is fully in ret
                {
                    ret += json[i];
                }
                else { //Array element is complete, store in array and reset element variable.
                    array.Add(ret);
                    ret = "";
                }
                i++;
            }
            return new KeyValuePair<List<String>, int>(array, i);
        }

        KeyValuePair<String, int> desterilizeNumber(String json, int startAt) {
            Console.WriteLine("Attempting Desterilization of Number");
            String ret = "";
            int i = startAt;
            while (json[i] != ',') {
                Console.WriteLine(json[i]);
                ret += json[i];
                i += 1;
            }
            return new KeyValuePair<String, int>(ret, i - 1);
        }

        KeyValuePair<String,int> desterilizeString(String json, int startAt) {
            Console.WriteLine("Attempting Desteriliziation of String starting at " + startAt);
            int i = startAt + 1;
            String str = "";
            while (json[i] != '"') {
                str += json[i];
                i += 1;
            }
            Console.WriteLine(str);
            Console.WriteLine("Resuming Main Loop @ character " + (i));
            return new KeyValuePair<String,int>(str, i);
        }

        public override string ToString()
        {
            return this.toString();
        }

        public String toString() {
            String ret = "{";
            int i = 0;
            foreach (KeyValuePair<String, JSONEntry> entry in dict) {
                if (entry.Value.data_type == "System.String".GetType())
                {
                    ret += "\"" + entry.Key + "\"" + ":" + "\"" + entry.Value.data.ToString() + "\"";
                }
                else if (entry.Value.data_type.ToString().Contains("System.Collections.Generic.List")) {
                    //Convert list to String Representation
                    String list_content = "[";
                    int ii = 0;
                    foreach (String val in entry.Value.data as List<String>) {
                        if (ii != (entry.Value.data as List<String>).Count - 1) {
                            //Covers 90% of strings. TODO: Add catch for rest of alphabet letters.
                            if (list_content.Contains("a") || list_content.Contains("e") || list_content.Contains("i") || list_content.Contains("o") || list_content.Contains("u") || list_content.Contains("y"))
                            {
                                list_content += "\"" + val + "\",";
                            }
                            else
                            {
                                list_content += val + ",";
                            }
                        }
                        else {
                            //Covers 90% of strings. TODO: Add catch for rest of alphabet letters.
                            if (list_content.Contains("a") || list_content.Contains("e") || list_content.Contains("i") || list_content.Contains("o") || list_content.Contains("u") || list_content.Contains("y"))
                            {
                                list_content += "\"" + val + "\"";
                            }
                            else
                            {
                                list_content += val;
                            }
                        }
                        ii += 1;
                    }
                    list_content += "]";

                    ret += "\"" + entry.Key + "\"" + ":" + list_content;
                }
                else
                {
                    ret += "\"" + entry.Key + "\"" + ":" + entry.Value.data.ToString();
                }
                if (i != dict.Count - 1) {
                    ret += ","; 
                }
                i += 1;
            }
            ret += "}";
            return ret;
        }
    }
}
